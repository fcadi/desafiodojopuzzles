﻿'use strict';
var express = require('express');
var router = express.Router();
var app = {};

/* GET users listing. */
router.get('/', function (req, res) {
    res.send('respond with a resource');
});


router.post('/', function (req, res) {
    var ret = app.palavras.enviarPalavras(req.body.palavras);
    res.send(ret);
});


app.palavras = (function() {
    'use strict';

    var retorno = [];
    var me = {
        enviarPalavras: enviaPalavras
    };

    function enviaPalavras(palavras) {
        // Limpa array de retorno
        retorno = [];

        // Lista das palavras enviadas
        var arrayOfStrings = palavras.split(',');


        arrayOfStrings.forEach(function(data) {
            var s = 0;
            var p = data;

            if (data.length > 0) {
            var palavraChar = palavraToChar(data);
            palavraChar.forEach(function(p) {
                s = s + isInArray(p);

            });

            retorno.push({
                'palavra': p,
                'nomeClasse': isPrimeNumber(s),
                'soma': s
            });
        }
        });


        return JSON.stringify(retorno);
    }

    // Retorna um array de caracteres de uma string
    function palavraToChar(parameters) {
        return parameters.split('');
    }

    // Checa se um valor é um numero primo
    function isPrimeNumber(n) {

        if (n === 1) {
            return 'noPrime';
        }
        else if (n === 2) {
            return 'isPrime';
        } else {
            for (var x = 2; x < n; x++) {
                if (n % x === 0) {
                    return 'noPrime';
                }
            }
            return 'isPrime';
        }
    }

    // Verifica a posição no array de cada letra
    function isInArray(value) {

        var listaLetras = [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ];

        // Retorna a o indice do caractere encontradao no array ( - )
        return listaLetras.indexOf(value) > -1 ? (listaLetras.indexOf(value)) +1 : 0;
    }


    return me;
})();

module.exports = router;
