﻿document.getElementById("btnEnviar").addEventListener("click", function (e) {
    e.preventDefault();
    var dados = document.getElementById('dadosParaEnviar').value;

    if (dados.length > 0) {

        
        var data = JSON.stringify({
            "palavras": dados
        });

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {

                mostrarResultado(JSON.parse(this.responseText));
            }
        });

        xhr.open("POST", "/palavras");
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader("cache-control", "no-cache");

        xhr.send(data);
    } else {
        alertaSemPalavras();
    }

});

// Alerta sem dados a enviar
function alertaSemPalavras() {
    document.getElementsByClassName('semDados')[0].style.display = 'block';


}

// 
function mostrarResultado(valores) {
    var ret = '<p>Palavras em <span class="isPrime">vermelho</span> tem somatória igual numero primo, demais não..</p>';
    var divResultado= document.getElementsByClassName('listaPalavras')[0];
    document.getElementsByClassName('semDados')[0].style.display = 'none';
    divResultado.style.display = 'none';

    divResultado.innerHTML = '';

    valores.forEach(function (d, i) {
        ret = ret + '<p><span class="'+d.nomeClasse+'">'+d.palavra+'<span></p>';
        
    });
    divResultado.innerHTML = ret;
    divResultado.style.display = 'block';

}

document.getElementById('dadosParaEnviar').addEventListener('keyup', function() {
    this.value = this.value.replace(/[^a-zA-Z,]/g, '');

});



